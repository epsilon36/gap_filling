import subprocess
import numpy as np
import os

class Mesh:
    """A class to store the mesh reading and ray casting methods
    Intpus: 
        meshfile-The path to the mesh
        size - The max distance from one side to the other
        resolution - The number of triangles in the mesh. Defaults to max triangles from original file
    """
        
    def __init__(self,meshFile,size,resolution=None):
        self.size = size

        #Run dessemation program
        if resolution !=None:
            self.meshFile = self.ReduceFaces(meshFile,Faces=resolution)
        else:
            self.meshFile = meshFile
        
        #returns a [P,3,3] array for [triangles,3Points,xyz]
        self.mesh = self.ReadMesh(self.meshFile)
                
    def ReadMesh(self,file):
        """Reads the .obj file and outputs the xyz coordinates of the triangles.
        Input: The directory to the obj file
        Output: A [3,3] array consisting of [points,xyz]
        """
        #Open file and dump lines within file into a list
        with open(file,'r') as f:
            lines = f.readlines()
        
        #Iterate through each line. Look at label in the line and add the fields within that line
            #to either the vertices or the triangles list depending upon the label.
        Vertices = []
        Triangles = []
        for line in lines:
            if line[0] == 'v':
                vertex = line.split()[1:4]
                Vertices.append(vertex)
            if line[0] == 'f':
                triangle = line.split()[1:4]
                #If textures are used, remove texture data
                if "//" in line:
                    T = []
                    for t in triangle:
                        t = t.split("//")[0]
                        T.append(t)
                    triangle = T
                Triangles.append(triangle)
        #Offset the Triangles so that they are Zero indexed.
        Triangles = np.array(Triangles,dtype=np.int32)-1
        
        #Pull the vertices of the indeces for each triangle to create a [3,3] array representing [Points,xyz]
        TrueVertices = []
        for tri in Triangles:
            for t in tri:
                TrueVertices.append(Vertices[t])
    
        #Convert lists to numpy array in proper shape
        TrueVertices = np.array(TrueVertices,dtype=np.float32)
        TrueVertices = np.reshape(TrueVertices,(-1,3,3))
        return TrueVertices
        
    def ReduceFaces(self,Input,i=0,Faces=2000):
        import random as rand
        #Pathway to executable
        reductionExe = r"C:\Users\Arren Bustamante\Documents\Simplify2.exe"
        #Pathway to Low Res Folder
        OutputFolder = r"C:\Users\Arren Bustamante\Documents\Data\Low Res"
        

        #Obtain the path to input file. (without double slashes)
        standardName = os.path.normpath(Input)
        #Obtain folder and file name
        folderName,fileName = standardName.split(os.sep)[-2:]
        #Construct the path for the output file for the retesselated mesh
        outFile = os.path.join(OutputFolder,folderName+" "+fileName.replace('.obj',' ')+"Res "+str(i)+".obj")
        #Create string to pass to desimation exe
        commandString = '"'+reductionExe+'"'+' '+'"'+Input+'"'+' '+'"'+outFile+'"'+' 1.0 7 '+str(Faces)
        #Call desimation exe to reduce number of faces. Return the path to the generated file.
        subprocess.call(commandString)
        return outFile
    
    def SegmentMesh(self,Ydim):
        """This function reduces the mesh by removing all but the chunck surrounding the desired cross section. This is 
        done to make rendering more efficient.
        Returns an [n,3,3] array consisting of [points_in_bounds,triangle_points,xyz]"""
        Vertices = self.mesh
        #Obtain box 4 mm thick
        YMin = Ydim-2
        YMax = Ydim+2
        
        #Find the min/max y dimension of each triangle
        v_ymax = np.max(Vertices[:,:,1],axis=1)
        v_ymin = np.min(Vertices[:,:,1],axis=1)
        
        #Create a binary list to tell which points are in/out of bounds.
        V_ymax = np.where(v_ymax>YMax,0,1)
        V_ymin = np.where(v_ymin<YMin,0,1)
        mask = V_ymax*V_ymin
        
        #Obtain indices of which points are in bounds
        nz = np.nonzero(mask)[0]
        #Obtain vertices from these indices.
        result = Vertices[nz]
        return result
        
        
        
    def Intersect(self,Ydim,n_rays):
        """Pulls points which represents a cross section of a mesh where the cross section plane is along the Y axis.
        Inputs:
                Ydim: The cross section of the Y coordinate
                n_rays: The number of rays to cast
                direction: Either 'x' or 'z' to represent which direction to cast the rays.
        Outputs:
            An array consisting of the intersection points of shape [intersections,3]
        """
        #[P,3,3]
        Vertices = self.SegmentMesh(Ydim)
#        Vertices = self.mesh
        
        #The number of triangles on the mesh
        M = Vertices.shape[0]
        
        #Create space along the x axis form which to project lines
        space = np.linspace(0,np.pi*2,n_rays)
        
        #Generate a center point
        v0 = np.zeros(3)+np.array([0,Ydim,0])
        #Create vectors radiating in a circle
        x = np.cos(space)
        y = np.zeros(space.shape[0])
        z = np.sin(space)
        vt = np.stack((x,y,z),axis=1)

        
        
        self.v0 = v0
        
        #Obtain the segments ab and bc of each triangle
        #[M,3]
        va = Vertices[:,0,:]
        vab = va-Vertices[:,1,:]
        vac = va-Vertices[:,2,:]
        
        #Take the cross product of the segments to define the normal vector
        #[M,3]
        N = np.cross(vab,vac)
        
        #[M,L,3]
        #Broadcast rays to the shape [M,L,3]
        V0 = np.broadcast_to(v0,(M,n_rays,3))
        Vt = np.broadcast_to(vt,(M,n_rays,3))
        
        #Broadcast Va (a somewhat arbitrary point on the triangle) to [M,L,3]
        Va = np.broadcast_to(np.expand_dims(va,1),(M,n_rays,3))

        #Calculate the parametric parameter "t" where the ray intersects the plane of the mesh triangle.
        #[M,L,3]
        numerator = -(V0-Va)
        #[M,L,1]
        denominator = np.expand_dims(np.einsum('ij,kj->ik',N,vt),-1)
        #[M,L]
        t = np.einsum('ijk,ik->ij',numerator/denominator,N)
        
        #Broadcast to [M,L,1]
        T = np.expand_dims(t,-1)
        
        #Remove the axis 2 of T. Create mask to eliminate negative values for T
            #These correspond to places where the mesh crosses the ray in the ray's opposite direction
        mask = np.where(T[:,:,0]>0,1,0)
        
        #Calculate point where the ray hit the mesh
        #[M,L,3]
        Vhit = V0+Vt*T
        
        #Find difference between Vhit and the vertices of the triangle
        #[M,L,3]
        VhA = Vhit-Va
        VhB = Vhit-np.broadcast_to(np.expand_dims(Vertices[:,1,:],1),(M,n_rays,3))
        VhC = Vhit-np.broadcast_to(np.expand_dims(Vertices[:,2,:],1),(M,n_rays,3))
        
        #Calculate the area of the triangle
        #[M,L]
        _TriArea = np.linalg.norm(np.cross(vab,vac),axis=1)
        TriArea = np.broadcast_to(np.expand_dims(_TriArea,1),(M,n_rays))
        
        #Compute barycentric coordinates
        #[M,L]
        Alpha = np.linalg.norm(np.cross(VhB,VhC),axis=2)/TriArea
        Beta = np.linalg.norm(np.cross(VhC,VhA),axis=2)/TriArea
        Gamma = np.linalg.norm(np.cross(VhB,VhA),axis=2)/TriArea
        
        #Remove nan values
        Alpha = np.where(np.isnan(Alpha),0,Alpha)
        Beta = np.where(np.isnan(Beta),0,Beta)
        Gamma = np.where(np.isnan(Gamma),0,Gamma)
        
        #Remove values outside the triangle
        Alpha[Alpha<0] = 0
        Beta[Beta<0]=0
        Total = Alpha+Beta+Gamma
        Total[Total<-.001] = 0
        Total[Total>1.001] = 0
        #Apply mask to total.
        Total = Total*mask
        
        
        #Any value that has not been removed (set to zero) is an intersection point.
            #Get the indices for these points as nzX and nzY
            
        #The non-zero argument reports items in row order. Since we want the indices to be returned counter-clockwise,
            #we must transpose "Total" so that the rows corresponds to the rays. Total.T --> [rays,triangles]
        nzY,nzX = np.nonzero(Total.T)
        #Exctract value of each array at the given indices
        V0s = V0[nzX,nzY]
        Vts = Vt[nzX,nzY]
        Ts = T[nzX,nzY]
        
        #Compute the xyz coordinates of the intersection point.
        Intersections = V0s+Vts*np.broadcast_to(Ts,(nzX.shape[0],3))
        

        return Intersections
    

    
def _Axis(points,axis,pixels,size):
    """Creates a single axis of the grid for pixelization
    Inputs:
        points-The points which make up the corss section [P,3]
        axis- wither 'x' or 'z' depending upon the cross section
        pixels-The number of pixels to use for rendering
        size - The max positive x or z dimension of the model
    Output:
        The indices along the given axis for which a given point lies [P,]
    """
    #The distance between pixels
    dp = size/pixels

    #Convert the axis name to the axis index
    aDict = {'x':0,'z':2}
    a = aDict[axis]
    
    #Create evenly spaced points to make up a grid axis
    grid = np.arange(pixels)*dp-size/2
    
    p = points.shape[0]
    g = grid.shape[0]
    
    #Make both the grid points and the list of points a [p,g] array
    Grid = np.broadcast_to(np.expand_dims(grid,0),(p,g))
    Points = np.broadcast_to(np.expand_dims(points[:,a],1),(p,g))
    
    #See if the point lies within the lower bound of a given pixel
    Bound0 = np.where(Grid<Points,True,False)
    #See if the point lies within the upper bound (pixel+dp) of a given pixel
    Bound1 = np.where(Grid+dp>=Points,True,False)
    
    #Equivalent to a logical AND
    Bound = Bound0*Bound1
    #Obtain the pixel index (along the given axis) for a given point.
    index = np.argmax(Bound,axis=1)
    
    return index
    
def Convolve(Image,c=.125,s=.125,i=0,K=None):
    """Perform a 2D Convolution on input image. 
    Inputs:Kernel in the form
        c|s|c
        s|i|s
        c|s|c
        
        or pass an array K"""
        
    q = .073235
    r = .176765
    Filters={"Gaussian":(1/16)*np.array([[1,2,1],[2,4,2],[1,2,1]]),"Sharpen":np.array([[0,-1,0],[-1,5,-1],[0,-1,0]]),"Diffuse":np.array([[q,r,q],[r,0,r],[q,r,q]])}
    from scipy.signal import convolve2d as cv
    if K is None:
        K = np.array([[c,s,c],[s,i,s],[c,s,c]])
    if type(K) == str:
        K = Filters[K]
    Filtered = cv(Image,K,"same")
    return Filtered

def Render(points,pixels,size=10):
    """Renders the a set of 3d points representing a cross section as an image. Note: Performs a Diffuse convolution on the output
    Inputs:
        points- The intersection points which to render. (Y values must be the same for all points since it is a cross section)
            The shape of this arguemnt should be [points,3].
        pixels-The number of pixels to render
        size - The max positive x or z dimension of the model

    Outputs:
        A [pixels,pixels] array representing of an image.
    """
   
    
    #The y axis gets flipped when rendered into an image. Flip the Y axis here to undo this.
    #Obtain the X and Y coordinates of each pixel
    Px = _Axis(points,'x',pixels,size)
    Py = -_Axis(points,'z',pixels,size)
    
    
    #Create a binary array where a 1 marks the cross section outline.
    Image = np.zeros((pixels,pixels))
    Image[Px,Py] = 1
    return Convolve(Image,K="Diffuse")
    

    
def Plot(Image):
    """Plot the image"""
    import matplotlib.pyplot as plt
    #Pyplot automatically transposes the image. We want to transpose it back.
#    Image = np.dot(Image,np.array([[-1,0],[0,1]]))
    plt.imshow(Image.T)
    plt.show()
    
    
    


        
####################Test Code##################################################


#file = r'D:\Gap Fill\Sample Data\Anterior\1.obj'
##file = r"D:\IsoHedron.obj"
#Ydim = 2
#pixels= 500
#size = 10
#obj = Mesh(file,size)
#intersections = obj.Intersect(Ydim,100)
#
#Image = Render(intersections,400,10)
#Plot(Image)
#Image[0:75,:] = 0
#Plot(Image)
#
#A = Image
#for i in range(3):
#    print(i)
#    A = Convolve(A)
#    Plot(A)
#
#a = intersections
#import matplotlib.pyplot as plt
#for i in range(100):
#    plt.xlim(-5,5)
#    plt.ylim(-5,5)
#    plt.scatter(a[0:i,0],a[0:i,2])
#    plt.show()
##


