import MeshClass as mc
import numpy as np

class LeastSquares:
    def __init__(self,deg=2):
        self.deg = deg
        
    def _Fourier(self,t,n):
        return np.exp(n*2*np.pi*complex(0,1)*t)
    
    def Fourier(self,t):
        N = self.deg
        F = []
        #Obtain negative coefficients
        for _n in range(N):
            n = _n+1
#            F.append(self._Fourier(t,n-N-1))
            F.append(self._Fourier(t,n-N-1))
        #obtain C0 coefficient    
        F.append(self._Fourier(t,0))
        #Obtain positive coefficients
        for _n in range(N):
            n = _n+1
            F.append(self._Fourier(t,n))
        return np.array(F).T
    
    def Solver(self,examples,knowns = None):
        t = np.linspace(0,1,examples.shape[0])
        
        if knowns is None:
            K = self.Fourier(t)
        else:
            K = knowns
        T = examples
        term1 = np.linalg.inv(np.dot(K.T,K))
        return np.dot(term1,np.dot(K.T,T))
    
def FTrace(coefficients,n_rays):
    """
    Creates a cross-section trace given a set of Fourier coefficients.
    Inputs:
        coefficients - A set of fourier coefficients.
        n_rays - The number of rays to cast
    Outputs:
        The coordinates of the cross section trace as complex numbers
    """
    deg = int((coefficients.shape[0]-1)/2)
    LSobject = LeastSquares(deg)
    t = np.linspace(0,1,n_rays)
    
    exponentials = LSobject.Fourier(t)
    transform = np.dot(exponentials,coefficients)
    return transform
           
    
def _Spectrum(Z,n_circles):
    """This function performs the copmlex fourier transform on a 2D harmonic
    Inputs:
        Z-A complex array representing the cross section boundary
        n_circles- The number of frequencies for the fourier transform
            (A positive and a negative version of each circle is created for each frequency)
    Outputs:
        transform - A complex array which ia a filtered fourier transform of the original. 
        coeffients - The Fourier constants of the circles which "draw" the boundary"""
        
    #Instantiate a Least Squares object with the degree of the fourier kernel
    LSobject = LeastSquares(n_circles)
    #Solve the Least squares given the input image to obtain a complex array of the fourier coefficients
    coefficients = LSobject.Solver(Z)
    #Obtain an array that goes from 0 to 1 that is the same length as Z.
    t = np.linspace(0,1,Z.shape[0])
    #Run t thorugh the Fourier kernel
    exponentials = LSobject.Fourier(t)
    #Multiply the Fourier coefficients by the Fourier kernel and sum the terms
    transform = np.dot(exponentials,coefficients)
    
    return transform,coefficients

def Spectrum(coordinates,n_circles,Ydim):
    """Perform the Fourier transform complete with filtering (high level)
    Inputs:
        coords - The intersection points between the mesh and the cross section plane.
        n_circles- The number of frequencies for the fourier transform
            (A positive and a negative version of each circle is created for each frequency)
        Ydim- The Y dimension of the cross section.

    Outputs:
        P - The XYZ coordinates created by the fourier transform.
        circleCoeff - The Fourier constants of the circles which "draw" the boundary
        complexRaw -Complex form that the raw coordinates.
        complexOut - Complex form of the output
        """
    
    #Obtain the x and z coordinates of the cross section and center them.
    _Coords = coordinates-np.average(coordinates,axis=0)
    #Convert the x,z coordinates to complex coordinates.
    Coords = np.stack((_Coords[:,0],_Coords[:,2]),axis=1)
    R = Coords[:,0]
    I = Coords[:,1]*complex(0,1)
    complexRaw = R+I
    
    #Perform the fourier transform
    complexOut,circleCoeff = _Spectrum(complexRaw,n_circles)
    
    #Convert the output back into xyz coordinates.
    X = complexOut.real
    Z = complexOut.imag
    Y = np.zeros(complexOut.shape[0])+Ydim
    P = np.stack((X,Y,Z),axis=-1)
    return P,circleCoeff,complexRaw,complexOut
    

def CXPlot(C,size):
    """Method for plotting a complex value on cartegian coordinates
    Inputs:
        C - A complex array
    Outputs: 
        ScatterPlot"""
    #Obtain the real and imaginary components of the complex array
    x = C.real
    y = C.imag
    import matplotlib.pyplot as plt
    #Set bounds to be a square and plot
    limit = int(size/2)
    plt.xlim(-limit,limit)
    plt.ylim(-limit,limit)
    plt.scatter(x,y)
    plt.show() 
########################Test functions############################################
#file = r'D:\Gap Fill\Sample Data\Anterior\1.obj'
#Ydim = 5
#size = 10
#n_circles = 10
#
#mesh = mc.Mesh(file,size)
#coordinates = mesh.Intersect(Ydim,500)
#
#
#for i in range(20):
#    _,_,complexRaw,complexOut = Spectrum(coordinates,i,Ydim)
##    CXPlot(complexRaw,10)
##    CXPlot(complexOut,10)
#    _error = abs(complexRaw-complexOut)
#    error = np.average(_error)
#    print(i,error)






