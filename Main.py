import Epicycles as ep
import MeshClass as mc
import numpy as np
import os
import pickle

######################Test functions############################################
file = r'D:\Gap Fill\Sample Data\Anterior\1.obj'
folder = r'D:\Gap Fill\Sample Data\Anterior'
pickleJar = r'D:\Gap Fill\Pickles\_5 snapshots'


files = os.listdir(folder)
size = 10

restart = bool(int(input('Reset references?\n0)No\n1)Yes')))

if restart:
    Images = []
    Circles = []
    UsedFiles = []
    for I in range(10):
        file = os.path.join(folder,files[I])
        mesh = mc.Mesh(file,10)
        for i in range(20):
            Ydim = .5*i
            intersections = mesh.Intersect(Ydim,100)
            if len(intersections) > 0:
                print(i)
                xyz,C,rawSpec,Trans = ep.Spectrum(intersections,10,Ydim)
                ep.CXPlot(Trans,10)
                image = mc.Render(intersections,500,10)
                mc.Plot(image)
                Images.append(image)
                Circles.append(C)
                UsedFiles.append(file)
                
    with open(pickleJar,'wb') as f:
        pickle.dump((Images,Circles,UsedFiles),f)
    print("Pickled")
    
if not restart:
    with open(pickleJar,'rb') as f:
        Images,Circles,UsedFiles, = pickle.load(f)
    print("Unpickled")
            
        
        
###############New File####################################################
file2 = r"D:\Gap Fill\Sample Data\Anterior\20.obj"
size = 10
Ydim = 5
mesh = mc.Mesh(file2,10)

crossSection = mesh.Intersect(Ydim,100)
originalImage = mc.Render(crossSection,500)


croppedCrossSection = np.concatenate((crossSection[0:50],crossSection[50:75]),axis=0)

crop_points,crop_C,crop_raw,crop_out =ep.Spectrum(croppedCrossSection,10,Ydim)
cropImage = mc.Render(croppedCrossSection,500,10)

allCircles = np.array(Circles)
delta = np.sum(abs(allCircles-crop_C),1)
index = np.argmin(delta)

mc.Plot(mc.Convolve(originalImage,K="Diffuse"))
mc.Plot(mc.Convolve(cropImage,K="Diffuse"))
mc.Plot(mc.Convolve(Images[index],K="Diffuse"))


import matplotlib.pyplot as plt
f = np.arange(-10,11)

plt.subplot(221)
plt.title("Crop Real")
plt.bar(f,crop_C.real)

plt.subplot(222)
plt.title("Crop Imag")

plt.bar(f,crop_C.imag)

plt.subplot(223)
plt.title("Train Real")

plt.bar(f,allCircles[index])

plt.subplot(224)
plt.title("Train Imag")
plt.bar(f,allCircles[index])

plt.subplots_adjust(hspace=.7)
plt.show()

f = np.arange(-10,11)

for i in range(len(Images)):
    print(i)

    plt.subplot(231)
    plt.title("Train Real")
    
    plt.bar(f,allCircles[i])
    
    plt.subplot(232)
    plt.title("Train Imag")
    plt.bar(f,allCircles[i])    
    
    plt.subplot(233)
    mc.Plot(mc.Convolve(Images[i],K="Gaussian"))

    plt.show()

#a = allCircles[174]
#b = allCircles[36]
#
#A = ep.FTrace(a,100)
#B = ep.FTrace(b,100)
#ep.CXPlot(A,10)
#ep.CXPlot(B,10)
#
#c = (a+b)/2
#C = ep.FTrace(c,100)
#ep.CXPlot(C,10)


############################Frequency Analysis###########################################
#C = np.array(Circles)
#Ctest = np.array(Circles).imag
#f = np.arange(-300,301)
#nfg = np.stack((np.zeros(Ctest.shape[1]),f,Ctest[0]),axis=1)
#
##for n in range(Cabs.shape[0]-1):
##    _nfg = np.stack((np.zeros(Cabs.shape[1])+n+1,f,Cabs[n+1]),axis=1)
##    nfg = np.concatenate((nfg,_nfg),axis=0)
#  
#for n in range(Ctest.shape[0]-1):
#    _nfg = np.stack((np.zeros(Ctest.shape[1])+n+1,f,Ctest[n+1]),axis=1)
#    nfg = np.concatenate((nfg,_nfg),axis=0)
#        